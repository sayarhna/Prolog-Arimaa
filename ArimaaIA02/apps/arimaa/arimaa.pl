﻿:- module(bot,
      [  get_moves/3
      ]).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Représentation des données %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Gamestate: [side, [captured pieces]]
% Board: [[row,col,piece_type,side],[row,col,piece_type,side],…]
%   /!\ Board de 0 à 7 pour les row/col
% Moves : [[[old_row,old_col],[new_row,new_col]],[…],[…],[…]]
% Piece : [row,col,piece_type,side]
% Move : [[old_row,old_col],[new_row,new_col]]



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% Faits %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% trap(Row, Col): vrai quand la case [row, col] est un piège.
trap(2, 2).
trap(5, 2).
trap(2, 5).
trap(5, 5).

% Limites du Board
first_row(0).
first_col(0).
last_row(7).
last_col(7).

% strength(PieceType, Strength): indique la force correspondant au type de pièce donné.
strength(rabbit, 0).
strength(cat, 1).
strength(dog, 2).
strength(horse, 3).
strength(camel, 4).
strength(elephant, 5).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% Prédicats %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Choix des mouvements %%
%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat get_moves(Moves, Gamestate, Board): retourne les 4 mouvements choisis pour ce tour, en fonction du Board donné.
% Le gamestate n'est pas utilisé. L'idée initiale était de faire une stratégie défensive et une stratégie offensive, et de changer selon l'état du GameState, mais nous n'avons pas eu le temps de l'implémenter.

get_moves(Moves, Gamestate, Board):-
    add_chosen_move([], Moves1, Board, Board1),
    add_chosen_move(Moves1, Moves2, Board1, Board2),
    add_chosen_move(Moves2, Moves3, Board2, Board3),
    add_chosen_move(Moves3, Moves, Board3, _).

get_moves([], _, _).

% Prédicat test_see_all_moves(OldMoves, Board): affiche tous les mouvements possibles pour ce tour.

% Gestion du push
test_see_all_moves(OldMoves, Board) :-
    last(LastMove, OldMoves),
    \+empty(LastMove),
    % Si le dernier mouvement est un push:
    move_push(LastMove, Board),
    % On récupère les mouvements de deuxième partie de push.
    get_push_second_move(AllMoves, LastMove, Board),
    % On les affiche.
    print_tab(AllMoves, 'AllMoves').

test_see_all_moves(OldMoves, Board):-
    all_moves(Board, AllBasicMoves),
    % On récupère tous les mouvements Pull qu'on peut effectuer grâce au dernier mouvement.
    last(LastMove, OldMoves),
    get_pull_moves(PullMoves, LastMove, Board),
    % On les ajoute aux mouvements "classiques".
    concat(AllBasicMoves, PullMoves, AllBasicAndPullMoves),
    % On récupère tous les mouvements Push qu'on peut effectuer (bouger une pièce adverse).
    get_push_first_moves(PushMoves, Board),
    % On les ajoute aux autres mouvements.
    concat(AllBasicAndPullMoves, PushMoves, AllMoves),
    % On les affiche.
    print_tab(AllMoves, 'AllMoves').

% Prédicat add_chosen_move(OldMoves, NewMoves, OldBoard, NewBoard): ajoute le mouvement choisi et met à jour le board.

% Gestion du push
add_chosen_move(OldMoves, NewMoves, OldBoard, NewBoard) :-
    last(LastMove, OldMoves),
    \+empty(LastMove),
    % Si le dernier mouvement est un push:
    move_push(LastMove, OldBoard),
    % On récupère les mouvements de deuxième partie de push.
    get_push_second_move(AllMoves, LastMove, OldBoard),
    % On les traite.
    choose_move(AllMoves, ChosenMove, OldBoard),
    update_board(ChosenMove, OldBoard, NewBoard),
    concat(OldMoves, [ChosenMove], NewMoves), !.
    
add_chosen_move(OldMoves, NewMoves, OldBoard, NewBoard):-
    all_moves(OldBoard, AllBasicMoves),
    % On récupère tous les mouvements Pull qu'on peut effectuer grâce au dernier mouvement.
    last(LastMove, OldMoves),
    get_pull_moves(PullMoves, LastMove, OldBoard),
    % On les ajoute aux mouvements "classiques".
    concat(AllBasicMoves, PullMoves, AllBasicAndPullMoves),
    % Si on est pas au dernier mouvement
    tab_length(MovesLength, OldMoves),
    MovesLength < 3,
    % On récupère tous les mouvements Push qu'on peut effectuer (bouger une pièce adverse).
    get_push_first_moves(PushMoves, OldBoard),
    % On les ajoute aux autres mouvements.
    concat(AllBasicAndPullMoves, PushMoves, AllTheMoves),
    % On enlève les mouvements qui nous font tourner en rond.
    get_moves_free_from_circles(AllMoves, OldMoves, AllTheMoves, OldBoard),
    % On les traite.
    choose_move(AllMoves, ChosenMove, OldBoard),
    update_board(ChosenMove, OldBoard, NewBoard),
    concat(OldMoves, [ChosenMove], NewMoves), !.

add_chosen_move(OldMoves, NewMoves, OldBoard, NewBoard):-
    all_moves(OldBoard, AllBasicMoves),
    % On récupère tous les mouvements Pull qu'on peut effectuer grâce au dernier mouvement.
    last(LastMove, OldMoves),
    get_pull_moves(PullMoves, LastMove, OldBoard),
    % On les ajoute aux mouvements "classiques".
    concat(AllBasicMoves, PullMoves, AllTheMoves),
    % On est au dernier mouvement donc pas de première partie de push.
    % On enlève les mouvements qui nous font tourner en rond.
    get_moves_free_from_circles(AllMoves, OldMoves, AllTheMoves, OldBoard),
    % On traite les mouvements.
    choose_move(AllMoves, ChosenMove, OldBoard),
    update_board(ChosenMove, OldBoard, NewBoard),
    concat(OldMoves, [ChosenMove], NewMoves), !.

add_chosen_move(OldMoves, OldMoves, OldBoard, OldBoard) :- !.

% Prédicat choose_move(AllMoves, ChosenMove, Board): retourne l'un des 4 mouvements choisis par l'IA pour ce tour.
choose_move(AllMoves, ChosenMove, Board):-
    moves_with_values(WithValues, AllMoves, Board),
    best_move(WithValues, [ChosenMove, _]).

% Prédicat get_moves_free_from_circles(AllCleanMoves, OldMoves, AllMoves, Board): retourne l'ensemble des mouvements qui ne nous font pas tourner en rond.
get_moves_free_from_circles([], _, [], _) :- !.

get_moves_free_from_circles([Move | TailCleanMoves], OldMoves, [Move | TailMoves], Board) :-
    \+goes_round_in_circles(OldMoves, Move, Board), !,
    get_moves_free_from_circles(TailCleanMoves, OldMoves, TailMoves, Board).

get_moves_free_from_circles(AllCleanMoves, OldMoves, [_ | TailMoves], Board) :-
    get_moves_free_from_circles(AllCleanMoves, OldMoves, TailMoves, Board), !.

% Prédicat goes_round_in_circles(OldMoves, Move, Board): indique si le mouvement nous fait tourner en rond.

% Si le troisième mouvement est le même que le premier, on tourne en rond.
goes_round_in_circles([Move1, _], Move1, _) :- !.

% Si le quatrième mouvement est le même que le deuxième, on tourne en rond.
goes_round_in_circles([_, Move2, _], Move2, _) :- !.

% Si le quatrième mouvement est le même que le premier et que le deuxième est le même que le troisième, on tourne en rond.
goes_round_in_circles([Move1, Move2, Move2], Move1, _) :- !.

% Si le quatrième mouvement est le même que le premier et qu'on a pas fait de pull entre-temps, on tourne en rond.
goes_round_in_circles([Move1, _, [_, [Move3Row, Move3Col]]], Move1, Board) :-
    get_piece_at_coord(Piece, Move3Row, Move3Col, Board),
    \+same_side(Piece, [_, _, _, gold]), !.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Obtention des mouvements possibles %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat all_moves_test(Board, AllMoves): prédicat de test qui appelle all_moves et affiche le Board.
all_moves_test(Board, AllMoves) :- all_moves(Board, AllMoves), print_tab(AllMoves, 'All Moves').

% Prédicat all_moves(Board, AllMoves): retourne tous les mouvements possibles.
all_moves(Board, AllMoves):-
    get_silver_pieces(Board, SilverPieces),
    add_moves(Board, SilverPieces, AllMoves).

% Prédicat add_moves(Board, SilverPieces, AllMoves) : ajoute à chaque étape les mouvements possibles pour une pièce silver à la liste AllMoves.
add_moves(_, [], []):-!.
add_moves(Board, [Piece|TailSilver], AllMoves):-
    get_moves_piece(Moves, _, Board, Piece),
    add_moves(Board, TailSilver, MovesRest),
    concat(Moves, MovesRest, AllMoves), !.

% Prédicat moves_direction(Piece, Direction, Move): retourne le tableau Move [coord depart, coord arrivée], en fonction de la position de la pièce et de la direction dans laquelle on veut la déplacer.
% /!\ n'effectue aucune vérification sur le mouvement en question
moves_direction([Row, Col, _, _], right, [[Row, Col],[ Row, C ]]):-
    C is Col + 1.

moves_direction([Row, Col, _, _], left, [[Row, Col],[ Row, C ]]):-
    C is Col - 1.

moves_direction([Row, Col, _, _], up, [[Row, Col],[ R, Col ]]):-
    R is Row - 1.

moves_direction([Row, Col, _, _], down, [[Row, Col],[ R, Col ]]):-
    R is Row + 1.

% Prédicat rabbit_moving_back(Piece, Move): vérifie si une pièce est un lapin et tente de reculer (ce qui est interdit).
rabbit_moving_back([Row, _, rabbit, _], [ [_, _], [NewRow, _] ]):-
    Row > NewRow.

% Prédicat get_moves_piece(Moves, GameState, Board, Piece): retourne les mouvements possibles à distance 1 pour cette pièce.

% On teste immédiatement si la pièce peut bouger (si elle est figée ou non).
get_moves_piece([], _, Board, Piece):-
    frozen(Piece, Board), !.

% On ajoute les mouvements possibles dans les 4 directions.
get_moves_piece(Moves, _, Board, Piece):-
    get_moves_piece_function(right, [], Moves1, Board, Piece),
    get_moves_piece_function(left, Moves1, Moves2, Board, Piece),
    get_moves_piece_function(up, Moves2, Moves3, Board, Piece),
    get_moves_piece_function(down, Moves3, Moves, Board, Piece).

% Prédicat get_moves_piece_function(Direction, OldMoves, NewMoves, Board, Piece): permet de retourner les mouvements possibles dans les 4 directions.
get_moves_piece_function(Direction, OldMoves, [Move|OldMoves], Board, Piece):-
    moves_direction(Piece, Direction, Move),
    test_move(Move, Piece, Board), !.

get_moves_piece_function(_, OldMoves, OldMoves, _, _).



%%%%%%%%%%%%%%%%%%
%% Push et Pull %%
%%%%%%%%%%%%%%%%%%

% Prédicat move_push(Move, Board): indique si le mouvement donné est une première partie de push (pousser l'adverse).
move_push([_, [NewRow, NewCol]], Board) :-
    % Si la pièce bougée n'est pas silver, le mouvement est la première partie d'un push.
    get_piece_at_coord(Piece, NewRow, NewCol, Board),
    \+same_side(Piece, [_, _, _, silver]).

% Prédicat get_push_second_move(PushSecondMoves, OldPushMove, Board): retourne le mouvement qui correspond à la deuxième partie de push du mouvement donné.
get_push_second_move(PushSecondMoves, [[OldRow, OldCol], _], Board) :-
    nearby_pieces(NearbyPieces, [OldRow, OldCol, _, _], Board),
    get_silver_pieces(NearbyPieces, SilverPieces),
    get_moves_towards_coord(PushSecondMoves, SilverPieces, [OldRow, OldCol]), !.
get_push_second_move([], [], _).

% Prédicat get_moves_towards_coord(Moves, Pieces, Coord): retourne les mouvements entre les pièces données et les coordonnées données.
get_moves_towards_coord([], [], _).
get_moves_towards_coord([[[FirstRow, FirstCol], Coord] | TailMoves], [[FirstRow, FirstCol, _, _] | TailPieces], Coord) :-
    get_moves_towards_coord(TailMoves, TailPieces, Coord), !.

% Prédicat get_pull_moves(PullMoves, LastMove, Board): retourne les deuxièmes parties de mouvements pull par rapport au dernier mouvement (première partie de mouvement pull).
get_pull_moves([], [], _) :- !.
get_pull_moves([], _, []) :- !.
get_pull_moves(PullMoves, [[OldRow, OldCol], NewCoord], Board) :-
    nearby_pieces(NearbyPieces, [OldRow, OldCol, _, _], Board),
    get_pull_moves_piece(PullMoves, NearbyPieces, [[OldRow, OldCol], NewCoord], Board), !.

% Prédicat get_pull_moves_piece(PullMoves, NearbyPieces, LastMove, Board): retourne les deuxièmes parties de mouvements pull par rapport au dernier mouvement (première partie de mouvement pull) et aux pièces à côté de celle qui a bougé.
get_pull_moves_piece([], _, [], _).
get_pull_moves_piece([], [], _, _).
get_pull_moves_piece(
        [[[NearbyPieceRow, NearbyPieceCol], [OldRow, OldCol]] | TailPullMoves],
        [[NearbyPieceRow, NearbyPieceCol, NearbyPieceType, NearbyPieceSide] | TailNearbyPieces],
        [[OldRow, OldCol], [NewRow, NewCol]],
        Board) :-
    % Si la pièce n'est pas silver:
    \+same_side([NearbyPieceRow, NearbyPieceCol, NearbyPieceType, NearbyPieceSide], [_, _, _, silver]),
    get_piece_at_coord(Piece, NewRow, NewCol, Board),
    % Si on est strictement plus fort que la pièce.
    is_stronger(Piece, [NearbyPieceRow, NearbyPieceCol, NearbyPieceType, NearbyPieceSide]),
    % Récursion
    get_pull_moves_piece(TailPullMoves, TailNearbyPieces, [[OldRow, OldCol], [NewRow, NewCol]], Board).

% Si on ne peux pas pull, on avance dans la liste.
get_pull_moves_piece(PullMoves, [_|TailNearbyPieces], LastMove, Board) :-
    get_pull_moves_piece(PullMoves, TailNearbyPieces, LastMove, Board).

% Prédicat get_push_first_moves(PushMoves, Board): retourne les premières parties de mouvement push possible à ce mouvement.
get_push_first_moves(PushMoves, Board) :-
    get_silver_pieces(Board, SilverPieces),
    get_push_first_moves_silver(SilverPushMoves, SilverPieces, Board),
    first(PushMovesTmp, SilverPushMoves),
    first(PushMoves, PushMovesTmp).

% Prédicat get_push_first_moves_silver(PushMoves, SilverPieces, Board): retourne les premières parties de mouvement push possible à ce mouvement avec les pièces silver données.
get_push_first_moves_silver([], [], _).

get_push_first_moves_silver(PushMoves, [SilverPiece | TailSilverPieces], Board) :-
    frozen(SilverPiece, Board),
    get_push_first_moves_silver(PushMoves, TailSilverPieces, Board).

get_push_first_moves_silver([PushMovesPiece | TailPushMoves], [SilverPiece | TailSilverPieces], Board) :-
    nearby_pieces(NearbyPieces, SilverPiece, Board),
    get_push_first_moves_nearby_pieces(PushMovesPiece, SilverPiece, NearbyPieces, Board),
    \+empty(PushMovesPiece),
    get_push_first_moves_silver(TailPushMoves, TailSilverPieces, Board).

get_push_first_moves_silver(PushMoves, [_ | TailSilverPieces], Board) :-
    get_push_first_moves_silver(PushMoves, TailSilverPieces, Board).

% Prédicat get_push_first_moves_nearby_pieces(PushMoves, SilverPiece, NearbyPieces, Board): retourne les premières parties de mouvement push possible à ce mouvement avec les pièces à côté données.
get_push_first_moves_nearby_pieces([], _, [], _).
get_push_first_moves_nearby_pieces([PushMoves | TailPushMoves], SilverPiece, [NearbyPiece | TailNearbyPieces], Board) :-
    % Si la pièce n'est pas silver:
    \+same_side(NearbyPiece, [_, _, _, silver]),
    % Si on est plus fort que la pièce:
    is_stronger(SilverPiece, NearbyPiece),
    % On récupère les mouvements de push possibles.
    get_push_first_moves_piece(PushMoves1, right, [], SilverPiece, NearbyPiece, Board),
    get_push_first_moves_piece(PushMoves2, left, PushMoves1, SilverPiece, NearbyPiece, Board),
    get_push_first_moves_piece(PushMoves3, up, PushMoves2, SilverPiece, NearbyPiece, Board),
    get_push_first_moves_piece(PushMoves, down, PushMoves3, SilverPiece, NearbyPiece, Board),
    get_push_first_moves_nearby_pieces(TailPushMoves, SilverPiece, TailNearbyPieces, Board).

get_push_first_moves_nearby_pieces(PushMoves, SilverPiece, [_ | TailNearbyPieces], Board) :-
    get_push_first_moves_nearby_pieces(PushMoves, SilverPiece, TailNearbyPieces, Board).

% Prédicat get_push_first_moves_piece(PushMoves, Direction, OldMoves, Piece, EnemyPiece, Board): retourne les premières parties de mouvement push possible à ce mouvement avec la pièce donnée et la pièce ennemie donnée.

% Ajouter une pièce à droite (Col+1, Row+0).
get_push_first_moves_piece(
        [[[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRow, EnemyPieceColRight]] | OldMoves],
        right, OldMoves, [PieceRow, PieceCol, _, _], [EnemyPieceRow, EnemyPieceCol, _, _], Board) :-
    EnemyPieceColRight is EnemyPieceCol + 1,
    in_board(EnemyPieceRow, EnemyPieceColRight),
    \+same_coord([EnemyPieceRow, EnemyPieceColRight], [PieceRow, PieceCol]),
    get_push_second_move(PushSecondMoves, [[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRow, EnemyPieceColRight]], Board),
    free_from_suicide(PushSecondMoves, Board),
    get_piece_at_coord(Piece, EnemyPieceRow, EnemyPieceColRight, Board),
    empty(Piece).

% Ajouter une pièce à gauche (Col-1, Row+0).
get_push_first_moves_piece(
        [[[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRow, EnemyPieceColLeft]] | OldMoves],
        left, OldMoves, [PieceRow, PieceCol, _, _], [EnemyPieceRow, EnemyPieceCol, _, _], Board) :-
    EnemyPieceColLeft is EnemyPieceCol - 1,
    in_board(EnemyPieceRow, EnemyPieceColLeft),
    \+same_coord([EnemyPieceRow, EnemyPieceColLeft], [PieceRow, PieceCol]),
    get_push_second_move(PushSecondMoves, [[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRow, EnemyPieceColLeft]], Board),
    free_from_suicide(PushSecondMoves, Board),
    get_piece_at_coord(Piece, EnemyPieceRow, EnemyPieceColLeft, Board),
    empty(Piece).

% Ajouter une pièce en bas (Col+0, Row+1).
get_push_first_moves_piece(
        [[[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRowDown, EnemyPieceCol]] | OldMoves],
        down, OldMoves, [PieceRow, PieceCol, _, _], [EnemyPieceRow, EnemyPieceCol, _, _], Board) :-
    EnemyPieceRowDown is EnemyPieceRow + 1,
    in_board(EnemyPieceRowDown, EnemyPieceCol),
    \+same_coord([EnemyPieceRowDown, EnemyPieceCol], [PieceRow, PieceCol]),
    get_push_second_move(PushSecondMoves, [[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRowDown, EnemyPieceCol]], Board),
    free_from_suicide(PushSecondMoves, Board),
    get_piece_at_coord(Piece, EnemyPieceRowDown, EnemyPieceCol, Board),
    empty(Piece).

% Ajouter une pièce en haut (Col+0, Row-1).
get_push_first_moves_piece(
        [[[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRowUp, EnemyPieceCol]] | OldMoves],
        up, OldMoves, [PieceRow, PieceCol, _, _], [EnemyPieceRow, EnemyPieceCol, _, _], Board) :-
    EnemyPieceRowUp is EnemyPieceRow - 1,
    in_board(EnemyPieceRowUp, EnemyPieceCol),
    \+same_coord([EnemyPieceRowUp, EnemyPieceCol], [PieceRow, PieceCol]),
    get_push_second_move(PushSecondMoves, [[EnemyPieceRow, EnemyPieceCol], [EnemyPieceRowUp, EnemyPieceCol]], Board),
    free_from_suicide(PushSecondMoves, Board),
    get_piece_at_coord(Piece, EnemyPieceRowUp, EnemyPieceCol, Board),
    empty(Piece).

get_push_first_moves_piece(Moves, _, Moves, _, _, _).

% Prédicat free_from_suicide(Moves, Board): indique si les mouvements donnés ne mènent pas à un suicide.
free_from_suicide(Moves, Board) :-
    !, \+full_on_suicides(Moves, Board).

% Prédicat full_on_suicides(Moves, Board): indique si les mouvements donnés mènent à un suicide.
full_on_suicides([], _) :-
    !, fail.
full_on_suicides([Move|_], Board) :-
    !, suicide(Move, Board).
full_on_suicides([_|TailMoves], Board) :-
    full_on_suicides(TailMoves, Board).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Récupérer les pièces Silver %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat get_silver_pieces(Board, SilverPieces): renvoie un tableau contenant les pièces Silver encore en jeu.
get_silver_pieces([],[]).

% tête silver
get_silver_pieces([ [Row, Col, Type, silver] | TailBoard ], [ [Row, Col, Type, silver] | TailSilverPieces ]):-
    get_silver_pieces(TailBoard, TailSilverPieces), !.

% tête gold
get_silver_pieces([ _ | TailBoard ], TailSilverPieces):-
    get_silver_pieces(TailBoard, TailSilverPieces).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Vérifie la validité du mouvement %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat test_move(Move, Piece, Board): indique si le mouvement est possible (coordonnées d'arrivée dans le Board, pas de pièce aux coordonnées d'arrivée, les lapins ne peuvent pas reculer).
% /!\ On ne donne à test_move que des mouvements autour de la pièce
test_move([ [OldRow, OldCol], [NewRow, NewCol] ], Piece, Board) :-
                in_board(NewRow, NewCol),
                % vérif que les lapins ne vont pas en arrière:
                \+rabbit_moving_back(Piece, [ [OldRow, OldCol], [NewRow, NewCol] ]),
                % vérif qu'il n'y a pas déjà une pièce à ces coordonnées:
                get_piece_at_coord(PieceResult, NewRow, NewCol, Board),
                empty(PieceResult),
                % vérif qu'on ne se suicide pas:
                \+suicide([ [OldRow, OldCol], [NewRow, NewCol] ], Board),
                % vérif qu'on ne fait pas mourir un ami:
                trapped_silver_friends(Board, TrappedFriends),
                never_gonna_give_you_up([[OldRow, OldCol], [NewRow, NewCol]], TrappedFriends, Board).



%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Informations, règles %%
%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat frozen(Piece, Board): indique si la pièce ne peut pas bouger.
frozen(Piece, Board) :- nearby_friend(Piece, Board), !, fail.
frozen(Piece, Board) :- nearby_pieces(Pieces, Piece, Board), at_least_one_stronger(Pieces, Piece).

% Prédicat at_least_one_stronger(Pieces, Piece): indique si au moins une pièce de Pieces est plus forte que Piece.
at_least_one_stronger([Piece1|_], Piece2) :- is_stronger(Piece1, Piece2), !.
at_least_one_stronger([_|TailPieces], Piece) :- at_least_one_stronger(TailPieces, Piece).

% Prédicat is_stronger(Piece1, Piece2): indique si Piece1 est strictement plus forte que Piece2.
is_stronger([_, _, Type1, _], [_, _, Type2, _]) :- strength(Type1, F1), strength(Type2, F2), F1 > F2.

% Prédicat is_same_strength(Piece1, Piece2): indique si Piece1 est de même force que Piece2.
is_same_strength([_, _, Type1, _], [_, _, Type2, _]) :- strength(Type1, F1), strength(Type2, F2), F1 == F2.

% Predicat nearby_friend(Piece, Board): indique si la piece donnée sur le plateau donné est à côté d'une piece amie.
nearby_friend(Piece, Board) :- nearby_pieces(Pieces, Piece, Board),
                    \+empty(Pieces),
                    at_least_one_friend(Pieces, Piece), !.

% Prédicat at_least_one_friend(Pieces, Piece): indique si au moins une pièce de Pieces est amie avec Piece.
at_least_one_friend([[_, _, _, Side]|_], [_, _, _, Side]) :- !.
at_least_one_friend([_|TailPieces], Piece) :- at_least_one_friend(TailPieces, Piece).

% Prédicat nearby_pieces(Pieces, Piece, Board): retourne un tableau des pieces voisines (up, down, right, left).
nearby_pieces([], _, []).
nearby_pieces(Pieces, Piece, Board) :- add_piece(right, Pieces1, Piece, Board, []),
                    add_piece(left, Pieces2, Piece, Board, Pieces1),
                    add_piece(up, Pieces3, Piece, Board, Pieces2),
                    add_piece(down, Pieces, Piece, Board, Pieces3).

% Prédicat add_piece(Direction, NewPieces, Piece, Board, Pieces): retourne un nouveau tableau NewPieces à partir de Pieces et y ajoute la pièce dans la direction donnée de Piece s'il y en a une.

% Ajouter une pièce à droite (Col+1, Row+0).
add_piece(right, [RightPiece|Pieces], [Row, Col, _, _], Board, Pieces) :- ColRight is Col+1,
                    in_board(Row, ColRight),
                    get_piece_at_coord(RightPiece, Row, ColRight, Board),
                    \+empty(RightPiece).
add_piece(right, Pieces, _, _, Pieces).

% Ajouter une pièce à gauche (Col-1, Row+0).
add_piece(left, [LeftPiece|Pieces], [Row, Col, _, _], Board, Pieces) :- ColLeft is Col-1,
                    in_board(Row, ColLeft),
                    get_piece_at_coord(LeftPiece, Row, ColLeft, Board),
                    \+empty(LeftPiece).
add_piece(left, Pieces, _, _, Pieces).

% Ajouter une pièce en bas (Col+0, Row+1).
add_piece(down, [DownPiece|Pieces], [Row, Col, _, _], Board, Pieces) :- RowBottom is Row+1,
                    in_board(RowBottom, Col),
                    get_piece_at_coord(DownPiece, RowBottom, Col, Board),
                    \+empty(DownPiece).
add_piece(down, Pieces, _, _, Pieces).

% Ajouter une pièce en haut (Col+0, Row-1).
add_piece(up, [UpPiece|Pieces], [Row, Col, _, _], Board, Pieces) :- RowTop is Row-1,
                    in_board(RowTop, Col),
                    get_piece_at_coord(UpPiece, RowTop, Col, Board),
                    \+empty(UpPiece).
add_piece(up, Pieces, _, _, Pieces).

% Predicat same_side(Piece1, Piece2): indique si les 2 pièces données sont du même côté.
same_side([_, _, _, Side], [_, _, _, Side]).

% Predicat get_piece_at_coord(Piece, Row, Col, Board): retourne la piece aux coordonnées données s'il y en a une, sinon retourne [].
get_piece_at_coord([], _, _, []) :- !.
get_piece_at_coord([Row, Col, Type, Side], Row, Col, [[Row, Col, Type, Side]|_]) :- !.
get_piece_at_coord(Piece, Row, Col, [_|TailBoard]) :- get_piece_at_coord(Piece, Row, Col, TailBoard), !.

% Prédicat same_coord([Row1, Col1], [Row2, Col2]): indique si les coordonnées données sont les mêmes.
same_coord([Row1, Col1], [Row2, Col2]) :- Row1 == Row2, Col1 == Col2.

% Predicat in_board(Row, Col): indique si une piece est dans le Board.
in_board(Row, Col) :- first_row(FR), Row >= FR,
                    first_col(FC), Col >= FC,
                    last_row(LR), Row =< LR,
                    last_col(LC), Col =< LC.

% Predicat trapped(Piece, Board): indique si la piece donnée meurt sur le plateau donné.
trapped([Row, Col, _, Side], Board) :- trap(Row, Col),
                    \+nearby_friend([Row, Col, _, Side], Board).

% Prédicat never_gonna_give_you_up(Move, TrappedFriends, Board): indique si on laisse mourir un ami avec ce mouvement (vrai si tout va bien).

% Si pas de pièce piégée, success.
never_gonna_give_you_up(_, [], _) :- !.

% Si à côté d'aucuns des amis piégés, success.
never_gonna_give_you_up([[OldRow, OldCol], _], TrappedFriends, _) :-
                    !, \+next_to_coord(TrappedFriends, OldRow, OldCol), !.

% Si à côté de la première pièce piégée ET que le mouvement rend cette pièce trapped, !, fail.
never_gonna_give_you_up([[OldRow, OldCol], NewCoord], [[TrappedRow, TrappedCol, _, _] | _], Board) :-
                    % Check si à côté de la première pièce piégée
                    next_to([TrappedRow, TrappedCol], [OldRow, OldCol]),
                    % Check si le mouvement mène à la mort de cette pièce
                    update_board([[OldRow, OldCol], NewCoord], Board, NewBoard),
                    get_piece_at_coord(TrappedPiece, TrappedRow, TrappedCol, NewBoard),
                    trapped(TrappedPiece, NewBoard),
                    % Si notre mouvement mène à la mort (trapped) de TrappedPiece, fail.
                    !, fail.

% Sinon, avancer dans la liste.
never_gonna_give_you_up(Move, [_ | TailTrappedFriends], Board) :-
                    !, never_gonna_give_you_up(Move, TailTrappedFriends, Board).

% Prédicat suicide(Move, Board): indique si le mouvement va tuer la pièce donnée.
suicide([[OldRow, OldCol], [NewRow, NewCol]], Board) :-
                    % Check si l'arrivée est un piège
                    trap(NewRow, NewCol),
                    % Check si le mouvement mène à la mort de cette pièce
                    update_board([[OldRow, OldCol], [NewRow, NewCol]], Board, NewBoard),
                    get_piece_at_coord(TrappedPiece, NewRow, NewCol, NewBoard),
                    empty(TrappedPiece).

% Prédicat next_to_coord(Coords, Row, Col): indique si les coordonnées données sont à côté d'une des paires de coordonnées données.
next_to_coord([[CoordRow, CoordCol] | _], Row, Col) :-
                    next_to([CoordRow, CoordCol], [Row, Col]), !.
next_to_coord([_|TailCoord], Row, Col) :-
                    next_to_coord(TailCoord, Row, Col).

% Prédicat next_to_pieces(Pieces, Row, Col): indique si les coordonnées données sont à côté d'une des pièces données.
next_to_pieces([[PieceRow, PieceCol, _, _] | _], Row, Col) :-
                    next_to([PieceRow, PieceCol], [Row, Col]), !.
next_to_pieces([_|TailPieces], Row, Col) :-
                    next_to_pieces(TailPieces, Row, Col).

% Prédicat next_to(Coord1, Coord2): indique si les coordonnées données sont à côté.

% next_to si la pièce est à droite (Col+1, Row+0)
next_to([Row, Col1], [Row, Col2]) :- Col2 is Col1+1.
next_to([Row, Col1], [Row, Col2]) :- Col1 is Col2+1.

% next_to si la pièce est à gauche (Col-1, Row+0)
next_to([Row, Col1], [Row, Col2]) :- Col2 is Col1-1.
next_to([Row, Col1], [Row, Col2]) :- Col1 is Col2-1.

% next_to si la pièce est en bas (Col+0, Row+1)
next_to([Row1, Col], [Row2, Col]) :- Row2 is Row1+1.
next_to([Row1, Col], [Row2, Col]) :- Row1 is Row2+1.

% next_to si la pièce est en haut (Col+0, Row-1)
next_to([Row1, Col], [Row2, Col]) :- Row2 is Row1-1.
next_to([Row1, Col], [Row2, Col]) :- Row1 is Row2-1.

% Prédicat trapped_silver_friends(Board, TrappedFriends): renvoie dans TrappedFriends la liste des coordonnées des pièces silver qui sont sur une case trap.
trapped_silver_friends(Board, TrappedFriends) :-
                    get_silver_pieces(Board, SilverPieces),
                    trapped_pieces(SilverPieces, TrappedFriends).

% Prédicat trapped_pieces(Pieces, TrappedPieces): renvoie dans TrappedPieces la liste des coordonnées des pièces dans celles données qui sont sur une case trap.
trapped_pieces([ [Row, Col, _, _] | TailPieces ], [ [Row, Col] | TrappedPieces]) :-
                    trap(Row, Col),
                    trapped_pieces(TailPieces, TrappedPieces), !.

trapped_pieces([_|TailPieces], TrappedPieces) :-
                    trapped_pieces(TailPieces, TrappedPieces), !.

trapped_pieces([], []).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Mise à jour du plateau %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat update_board(Move, Board, NewBoard): modifie le board en fonction du Move donné et retourne NewBoard. Modifie la pièce bougée et supprime la pièce piégée q'il y en a une.
update_board([[OldRow, OldCol], [NewRow, NewCol]], Board, NewBoard) :-
        trap(NewRow, NewCol),
        update_board_body([[OldRow, OldCol], [NewRow, NewCol]], Board, TmpBoard),
        get_piece_at_coord(Piece, NewRow, NewCol, TmpBoard),
        remove_piece_if_trapped(NewBoard, Piece, TmpBoard), !.

update_board([[OldRow, OldCol], [NewRow, NewCol]], Board, NewBoard) :-
        trap(TrapRow, TrapCol),
        next_to_coord([[OldRow, OldCol]], TrapRow, TrapCol),
        update_board_body([[OldRow, OldCol], [NewRow, NewCol]], Board, TmpBoard),
        get_piece_at_coord(Piece, TrapRow, TrapCol, TmpBoard),
        remove_piece_if_trapped(NewBoard, Piece, TmpBoard), !.

update_board([[OldRow, OldCol], [NewRow, NewCol]], Board, NewBoard) :-
        update_board_body([[OldRow, OldCol], [NewRow, NewCol]], Board, NewBoard), !.

% Prédicat update_board_body(Move, Board, NewBoard): modifie le board donné en fonction du Move donné et retourne NewBoard.
update_board_body(_, [], []).
update_board_body([ [OldRow, OldCol], [NewRow, NewCol] ],
        [ [OldRow, OldCol, PieceType, Side] | TailBoard ],
        [ [NewRow, NewCol, PieceType, Side] | TailBoard ]) :- !.
update_board_body(Move,
        [ TeteBoard | TailBoard ],
        [ TeteBoard | NewBoard ] ) :- update_board_body(Move, TailBoard, NewBoard).

% Prédicat update_board_aff(Move, Board, NewBoard): version debug de update_board avec affichage à la fin.
update_board_aff(Move, Board, NewBoard) :- update_board(Move, Board, NewBoard), print_tab(NewBoard).

% Prédicat remove_piece_if_trapped(NewBoard, Piece, OldBoard): supprime la piève donnée du board si elle est piégée.
remove_piece_if_trapped(NewBoard, Piece, OldBoard) :-
        trapped(Piece, OldBoard),
        remove_piece(Piece, OldBoard, NewBoard).
remove_piece_if_trapped(Board, _, Board).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Prédicats d'évaluation %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat moves_with_values(WithValues, AllMoves, Board): retourne une liste WithValues qui contient les mouvements ainsi que leur valeur.
moves_with_values([], [], _):-!.
moves_with_values([[Move, Value] | QueueWithValues], [Move|QueueAllMoves], Board):-
    evaluate(Board, Move, Value),
    moves_with_values(QueueWithValues, QueueAllMoves, Board).

% Prédicat evaluate(Board, Move, Value): retourne une valeur pour le mouvement donné.
evaluate(Board, [[OldRow, OldCol],[NewRow, NewCol]], Value):-
    get_piece_at_coord(Piece, OldRow, OldCol, Board),
    % exclu la pièce à tester du board
    remove_piece(Piece, Board, NewBoard),
    eval_win(Piece, [[OldRow, OldCol],[NewRow, NewCol]], 0, V1),
    eval_freeze_enemy(NewBoard, Piece, [_,[NewRow, NewCol]], V1, V2),
    eval_rabbit_forward(Piece, [_,[NewRow, _]], V2, V3),
    eval_protect_rabbit(Piece, [_,[NewRow, NewCol]], NewBoard, V3, V4),
    eval_towards_friend(Piece, [_,[NewRow, NewCol]], NewBoard, V4, V5),
    eval_push_to_death(Piece, [[OldRow, OldCol],[NewRow, NewCol]], NewBoard, V5, V6),
    eval_push_to_trap(Piece, [_,[NewRow, NewCol]], V6, V7),
    eval_silver_to_trap(Piece,[_,[NewRow, NewCol]], V7, V8),
    eval_forward(Piece, [[OldRow, _],[NewRow, _]], V8, V9),
    eval_rabbit_near_end(Piece, V9, Value)
    .

% Prédicat eval_win(Move, OldValue, NewValue): on ajoute 100 si le mouvement permet de gagner, pour être sûr qu'il sera choisi.
eval_win([_,_,rabbit,silver], [_,[NewRow, _]], OldValue, NewValue):-
    NewRow == 7,
    NewValue is OldValue + 100,
    !.
eval_win(_, _, OldValue, OldValue).

% Prédicat eval_freeze_enemy(Board, Piece, Move, OldValue, NewValue): si on freeze un ennemi avec le mouvement donné, on ajoute 10.
eval_freeze_enemy(Board, [_,_,Type,silver], [_,[Row, Col]], OldValue, NewValue):-
    nearby_pieces(Pieces, [Row, Col, _, _], Board),
    check_freeze_ennemy(Pieces, Type, Board),
    NewValue is OldValue + 10,
    !.
eval_freeze_enemy(_, _, _, OldValue, OldValue).

% Prédicat check_freeze_ennemy(Pieces, Type, Board): retourne vrai si la liste Pieces contient des pièces ennemies blocables avec une pièce silver de type Type.
% si ennemi qu'on peut freeze:
check_freeze_ennemy([ [Row, Col, TypeEnnemy, gold] |_], Type, Board):-
    is_stronger([_,_,Type,_], [_,_, TypeEnnemy, _]),
    \+nearby_friend([Row, Col, TypeEnnemy, gold], Board), !.
% sinon on continue à chercher:
check_freeze_ennemy([[_,_,_,silver]|Q], Type, Board):-
    check_freeze_ennemy(Q, Type, Board).

% Prédicat eval_rabbit_forward(Piece, Move, OldValue, NewValue): value l'avancée d'un lapin.
eval_rabbit_forward([OldRow, _, rabbit, silver], [_,[NewRow, _]], OldValue, NewValue):-
    OldRow < NewRow,
    NewValue is OldValue + 15,
    !.
eval_rabbit_forward(_, _, OldValue, OldValue).

% Prédicat eval_forward(Piece, Move, OldValue, NewValue): value l'avancée d'une pièce silver.
eval_forward([_,_,_,silver], [[OldRow, _],[NewRow, _]], OldValue, NewValue):-
    OldRow < NewRow,
    NewValue is OldValue + 10,
    !.
eval_forward(_, _, OldValue, OldValue).

% Prédicat eval_protect_rabbit(Move, Board, OldValue, NewValue): value le mouvement si la pièce vient protéger un lapin en se plaçant à ses côtés.
eval_protect_rabbit([_,_,_,silver],[_,[Row, Col]], Board, OldValue, NewValue):-
    nearby_pieces(Pieces, [Row, Col, _, _], Board),
    rabbit_inside(Pieces),
    NewValue is OldValue + 10,
    !.
eval_protect_rabbit(_, _, _, OldValue, OldValue).

% Prédicat rabbit_inside(List): vrai s'il y a un lapin dans la liste donnée.
rabbit_inside([[_,_,rabbit,_]|_]):-!.
rabbit_inside([_|Q]):-
    rabbit_inside(Q).

% Prédicat eval_towards_friend(Move, Board, OldValue, NewValue): value le mouvement s'il positionne la pièce à côté d'un ami.
eval_towards_friend([_,_,_,silver],[_,[Row, Col]], Board, OldValue, NewValue):-
    nearby_friend([Row, Col, _, silver], Board),
    NewValue is OldValue + 10,
    !.
eval_towards_friend(_, _, _, OldValue, OldValue).

% Prédicat eval_push_to_death(Piece, Move, Board, OldValue, NewValue): value le mouvement si on push un ennemi jusqu'à sa mort.
eval_push_to_death([_,_,_,gold], [OldCoord,[NewRow, NewCol]], Board, OldValue, NewValue) :-
    trap(NewRow, NewCol),
    update_board([OldCoord, [NewRow, NewCol]], Board, NewBoard),
    get_piece_at_coord(Piece, NewRow, NewCol, NewBoard),
    empty(Piece),
    NewValue is OldValue + 50,
    !.
eval_push_to_death(_,_,_, OldValue, OldValue).

% Prédicat eval_push_to_trap(Piece, Move, Board, OldValue, NewValue): value le mouvement si on push un ennemi sur un piège.
eval_push_to_trap([_,_,_,gold], [_,[NewRow, NewCol]], OldValue, NewValue) :-
    trap(NewRow, NewCol),
    NewValue is OldValue + 30,
    !.
eval_push_to_trap(_,_, OldValue, OldValue).

% Prédicat eval_silver_to_trap(Piece, Move, OldValue, NewValue): value le mouvement s'il met une de nos pièces sur un piège.
eval_silver_to_trap([_, _, elephant, silver], [_, [NewRow, NewCol]], OldValue, NewValue) :-
    trap(NewRow, NewCol),
    NewValue is OldValue - 20.
eval_silver_to_trap([_, _, _, silver], [_, [NewRow, NewCol]], OldValue, NewValue) :-
    trap(NewRow, NewCol),
    NewValue is OldValue - 10.
eval_silver_to_trap(_, _, OldValue, OldValue).

% Prédicat eval_rabbit_near_end(Piece, OldValue, NewValue): value le mouvement d'un lapin proche de gagner.
eval_rabbit_near_end([4, _, rabbit, silver], OldValue, NewValue) :-
    NewValue = OldValue + 10.
eval_rabbit_near_end([5, _, rabbit, silver], OldValue, NewValue) :-
    NewValue = OldValue + 20.
eval_rabbit_near_end([6, _, rabbit, silver], OldValue, NewValue) :-
    NewValue = OldValue + 30.
eval_rabbit_near_end(_, OldValue, OldValue).

% Prédicat best_move(WithValues, BestMove): retourne l'un des meilleurs mouvements possibles, selon leur valuation.
best_move([ [Move, Value] ],[Move, Value]).

best_move([ [MoveHead, ValueHead] | Tail], Best) :-
    best_move(Tail, [_, ValueTail]),
    ValueHead > ValueTail,
    Best = [MoveHead, ValueHead].

best_move([ [_, ValueHead] |Tail], Best) :-
    best_move(Tail, [MoveTail, ValueTail]),
    ValueHead =< ValueTail,
    Best = [MoveTail, ValueTail].



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Prédicats purement pratiques %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prédicat print_tab(Tab): affiche le tableau donné.
print_tab_loop([]) :- write(']').
print_tab_loop([T|Q]) :- empty(Q), format('~w', [T]), print_tab_loop(Q).
print_tab_loop([T|Q]) :- \+empty(Q), format('~w,\n', [T]), print_tab_loop(Q).
print_tab([T|Q]) :- format('Board: \n[~w,\n', [T]), print_tab_loop(Q).
print_tab([]) :- write('Board: []').

% Prédicat print_tab(Tab, Title): affiche le tableau donné avec le titre donné.
print_tab([T|Q], Title) :- format('~w: \n[~w,\n', [Title, T]), print_tab_loop(Q).
print_tab([], Title) :- format('~w: []', Title).

% Predicat empty(Tab): indique si le tableau donné est vide.
empty([]).

% Prédicat last(Element, Tab): renvoie le dernier élément du tableau donné.
last([], []) :- !.
last(E, [E|Q]) :- empty(Q), !.
last(E, [_|Q]) :- last(E, Q), !.

% Prédicat first(Element, Tab): renvoie le premier élément du tableau donné.
first([], []).
first(E, [E|_]).

% Prédicat concat(L1, L2, Resultat): concatène 2 listes.
concat([], L, L) :- !.
concat(L, [], L) :- !.
concat([T|Q], L, [T|Resultat]) :- concat(Q, L, Resultat).

% Prédicat tab_length(Length, Tab): renvoie la longueur du tableau donné.
tab_length(0, []).
tab_length(X, [_|Q]) :- tab_length(Y, Q), X is Y+1.

% Predicat clear(): nettoie la console.
clear :- write('\e[2J').

% Prédicat remove_piece(Piece, Board, NewBoard): retire une pièce du Board (on suppose qu'elle n'y est qu'une fois maximum).
remove_piece([], Board, Board).
remove_piece(Piece, [Piece|TailBoard], TailBoard) :- !.
remove_piece(Piece, [CurrPiece|TailBoard], [CurrPiece|NewBoard]):- remove_piece(Piece, TailBoard, NewBoard).

% Pour tester une opération longue :
%   statistics(walltime, [TimeSinceStart | [TimeSinceLastCall]]),
%   some_heavy_operation,
%   statistics(walltime, [NewTimeSinceStart | [ExecutionTime]]),
%   write('Execution took '), write(ExecutionTime), write(' ms.'), nl.
