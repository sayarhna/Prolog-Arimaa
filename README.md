# Prolog-Arimaa
Nous avons codé une Intelligence Artificielle en Prolog pour combattre le joueur dans ce jeu d'Arimaa.
Voici une courte documentation sur notre code Prolog.

Aperçu du plateau de jeu  
--------------> Colonnes  
'  
'  
'  
'  
V  
Lignes  



## Faits du jeu Arimaa

`trap(Row, Col)`
- vrai quand la case [Row, Col] est un piège.

`first_row(Row)`
- vrai pour le numéro de la première ligne (0).

`first_col(Col)`
- vrai pour le numéro de la première colonne (0).

`last_row(Row)`
- vrai pour le numéro de la dernière ligne (7).

`last_col(Col)`
- vrai pour le numéro de la dernière colonne (7).

`force(PieceType, Force)`
- indique la Force correspondant au type de pièce donné.


## Prédicats spécifiques au jeu Arimaa

### Choix des mouvements possibles

`get_moves(Moves, Gamestate, Board)`
- retourne les 4 mouvements choisis pour ce tour, en fonction du Board donné.

`add_chosen_move(OldMoves, NewMoves, OldBoard, NewBoard)`
- ajoute le mouvement choisi et met à jour le board.

`test_see_all_moves(OldMoves, Board)`
- affiche tous les mouvements possibles pour ce tour (prédicat de test).

`choose_movement(AllMoves, ChosenMove)`
- retourne l'un des 4 mouvements choisis par l'IA pour ce tour.

`get_moves_free_from_circles(AllCleanMoves, OldMoves, AllMoves, Board)`
- retourne l'ensemble des mouvements qui ne nous font pas tourner en rond.

`goes_round_in_circles(OldMoves, Move, Board)`
- indique si le mouvement nous fait tourner en rond.

### Obtention des mouvements possibles

`all_moves(Board, AllMoves)`
- retourne tous les mouvements possibles pour les pièces silver.

`all_moves_test(Board, AllMoves)`
- prédicat de test qui appelle `all_moves` et affiche le Board.

`add_moves(Board, SilverPieces, AllMoves)`
- ajoute à chaque étape les mouvements possibles pour une pièce silver à la liste AllMoves.

`moves_direction(Piece, Direction, Move)`
- retourne le tableau Move [coord depart, coord arrivée], en fonction de la position de la pièce et de la direction dans laquelle on veut la déplacer.
- /!\ n'effectue aucune vérification sur le mouvement en question

`rabbit_moving_back(Piece, Move)`
- vérifie si une pièce est un lapin et tente de reculer (ce qui est interdit).

`get_moves_piece(Moves, GameState, Board, Piece)`
- retourne les mouvements possibles à distance 1 pour cette pièce.

`get_moves_piece_function(Direction, OldMoves, NewMoves, Board, Piece)`
- permet de retourner les mouvements possibles dans les 4 directions.

### Gestion du push/pull

`move_push(Move, Board)`
- indique si le mouvement donné est une première partie de push (pousser l'adverse).

`get_push_second_move(PushSecondMoves, OldPushMove, Board)`
- retourne le mouvement qui correspond à la deuxième partie de push du mouvement donné.

`get_moves_towards_coord(Moves, Pieces, Coord)`
- retourne les mouvements entre les pièces données et les coordonnées données.

`get_pull_moves(PullMoves, LastMove, Board)`
- retourne les deuxièmes parties de mouvements pull par rapport au dernier mouvement (première partie de mouvement pull).

`get_pull_moves_piece(PullMoves, NearbyPieces, LastMove, Board)`
- retourne les deuxièmes parties de mouvements pull par rapport au dernier mouvement (première partie de mouvement pull) et aux pièces à côté de celle qui a bougé.

`get_push_first_moves(PushMoves, Board)`
- retourne les premières parties de mouvement push possible à ce mouvement.

`get_push_first_moves_silver(PushMoves, SilverPieces, Board)`
- retourne les premières parties de mouvement push possible à ce mouvement avec les pièces silver données.

`get_push_first_moves_nearby_pieces(PushMoves, SilverPiece, NearbyPieces, Board)`
- retourne les premières parties de mouvement push possible à ce mouvement avec les pièces à côté données.

`get_push_first_moves_piece(PushMoves, Direction, OldMoves, Piece, EnemyPiece, Board)`
- retourne les premières parties de mouvement push possible à ce mouvement avec la pièce donnée et la pièce ennemie donnée.

`free_from_suicide(Moves, Board)`
- indique si les mouvements donnés ne mènent pas à un suicide.

`full_on_suicides(Moves, Board)`
- indique si les mouvements donnés mènent à un suicide.

### Récupérer les pièces silver

`get_silver_pieces(Board, SilverPieces)`
- renvoie un tableau contenant les pièces Silver encore en jeu.

### Vérifier la validité du mouvement

`test_move(Move, Piece, Board)`
- indique si le mouvement est possible (coordonnées d'arrivée dans le Board, pas de pièce aux coordonnées d'arrivée, les lapins ne peuvent pas reculer).

### Informations et règles du jeu

`frozen(Piece, Board)`
- indique si la pièce ne peut pas bouger.

`at_least_one_stronger(Pieces, Piece)`
- indique si au moins une pièce de Pieces est plus forte que Piece.

`is_stronger(Piece1, Piece2)`
- indique si Piece1 est strictement plus forte que Piece2.

`is_same_strength(Piece1, Piece2)`
- indique si Piece1 est de même force que Piece2.

`nearby_friend(Piece, Board)`
- indique si la piece donnée sur le plateau donné est à côté d'une piece amie.

`at_least_one_friend(Pieces, Piece)`
- indique si au moins une pièce de Pieces est amie avec Piece.

`nearby_pieces(Pieces, Piece, Board)`
- retourne un tableau des pieces voisines (up, down, right, left).

`add_piece(Direction, NewPieces, Piece, Board, Pieces)`
- retourne un nouveau tableau NewPieces à partir de Pieces et y ajoute la pièce dans la direction donnée de Piece s'il y en a une.

`same_side(Piece1, Piece2)`
- indique si les 2 pièces données sont du même côté.

`get_piece_at_coord(Piece, Row, Col, Board)`
- retourne la piece aux coordonnées données s'il y en a une, sinon retourne [].

`same_coord([Row1, Col1], [Row2, Col2])`
- indique si les coordonnées données sont les mêmes.

`in_board(Row, Col)`
- indique si une piece est dans le Board.

`trapped(Piece, Board)`
- indique si la piece donnée meurt sur le plateau donné.

`never_gonna_give_you_up(Move, TrappedFriends, Board)`
- indique si on laisse mourir un ami avec ce mouvement (vrai si tout va bien).

`suicide(Move, Board)`
- indique si le mouvement va tuer la pièce donnée.

`next_to_coord(Coords, Row, Col)`
- indique si les coordonnées données sont à côté d'une des paires de coordonnées données.

`next_to_pieces(Pieces, Row, Col)`
- indique si les coordonnées données sont à côté d'une des pièces données.

`next_to(Coord1, Coord2)`
- indique si les coordonnées données sont à côté.

`trapped_silver_friends(Board, TrappedFriends)`
- renvoie dans TrappedFriends la liste des coordonnées des pièces silver qui sont sur une case trap

### Mise à jour du plateau

`update_board(Move, Board, NewBoard)`
- modifie le board en fonction du Move donné et retourne NewBoard. Modifie la pièce bougée et supprime la pièce piégée q'il y en a une.

`update_board_body(Move, Board, NewBoard)`
- modifie le board donné en fonction du Move donné et retourne NewBoard.

`update_board_aff(Move, Board, NewBoard)`
- version debug de `update_board` avec affichage à la fin.

`remove_piece_if_trapped(NewBoard, Piece, OldBoard)`
- supprime la piève donnée du board si elle est piégée.

## Prédicats d'évaluation

`moves_with_values(WithValues, AllMoves, Board)`
- retourne une liste WithValues qui contient les mouvements ainsi que leur valeur.

`evaluate(Board, Move, Value)`
- retourne une valeur pour le mouvement donné.

`eval_win(Move, OldValue, NewValue)`
- on ajoute 100 si le mouvement permet de gagner, pour être sûr qu'il sera choisi.

`eval_freeze_enemy(Board, Piece, Move, OldValue, NewValue)`
- si on freeze un ennemi avec le mouvement donné, on ajoute 10.

`check_freeze_ennemy(Pieces, Type, Board)`
- retourne vrai si la liste Pieces contient des pièces ennemies blocables avec une pièce silver de type Type.

`eval_rabbit_forward(Piece, Move, OldValue, NewValue)`
- value l'avancée d'un lapin.

`eval_protect_rabbit(Move, Board, OldValue, NewValue)`
- value le mouvement si la pièce vient protéger un lapin en se plaçant à ses côtés.

`rabbit_inside(List)`
- vrai s'il y a un lapin dans la liste donnée.

`eval_towards_friend(Move, Board, OldValue, NewValue)`
- value le mouvement s'il positionne la pièce à côté d'un ami.

`eval_push_to_death(Piece, Move, Board, OldValue, NewValue)`
- value le mouvement si on push un ennemi jusqu'à sa mort.

`eval_push_to_trap(Piece, Move, Board, OldValue, NewValue)`
- value le mouvement si on push un ennemi sur un piège.

`eval_silver_to_trap(Piece, Move, OldValue, NewValue)`
- value le mouvement s'il met une de nos pièces sur un piège.

`eval_rabbit_near_end(Piece, OldValue, NewValue)`
- value le mouvement d'un lapin proche de gagner.

`best_move(WithValues, BestMove)`
- retourne l'un des meilleurs mouvements possibles, selon leur valuation.

## Prédicats généraux

`print_tab(Tab)`
- affiche le tableau donné.

`print_tab(Tab, Title)`
- affiche le tableau donné avec le titre donné.

`empty(Tab)`
- indique si le tableau donné est empty.

`last(Element, Tab)`
- renvoie le dernier élément du tableau donné.

`first(Element, Tab)`
- renvoie le premier élément du tableau donné.

`concat(L1, L2, Resultat)`
- concatène 2 listes.

`tab_length(Length, Tab)`
- renvoie la longueur du tableau donné.

`clear()`
- nettoie la console.

`remove_piece(Piece, Board, NewBoard)`
- retire une pièce du Board (on suppose qu'elle n'y est qu'une fois maximum).
